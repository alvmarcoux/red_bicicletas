var map = L.map('main_map').setView([25.4497224,-100.8516929], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([25.4491523,-100.8475396]).addTo(map);
// L.marker([25.444139,-100.7995242]).addTo(map);
// L.marker([25.4503973,-100.8823369]).addTo(map);

$.ajax({
    dataType: "json", 
    url:"api/bicicletas", 
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})

