var express = require('express'); 
var router = express.Router();
var usuarioController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioController.usuarios_list);
router.post('/create', usuarioController.usuario_create);
// router.delete('/delete', usuarioController.bicicleta_delete);
// router.post('/:id/update', usuarioController.bicicleta_update);

module.exports = router;