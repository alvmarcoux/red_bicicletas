var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing bicicletas', function() {
    beforeAll(function(done) {
        mongoose.disconnect(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, {useNewUrlParser: true});

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function() {
                console.log('We are connected to test database:');
                done();
            });
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [25.25, -100.103]);

            expect(bici.code).toBe(1); 
            expect(bici.color).toBe("verde"); 
            expect(bici.modelo).toBe("urbana"); 
            expect(bici.ubicacion[0]).toEqual(25.25); 
            expect(bici.ubicacion[1]).toEqual(-100.103); 
            done();
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            let aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code); 

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0); 

                let aBici = new Bicicleta({code: 1, color:"verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    let aBici2 = new Bicicleta({code: 2, color:"rojo", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('debe eliminar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0); 

                let aBici = new Bicicleta({code:1, color:"verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);

                    let aBici2 = new Bicicleta({code: 2, color:"rojo", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.removeByCode(1, function(error, result){
                            Bicicleta.allBicis(function(err, bicis) {
                                expect(bicis.length).toEqual(1);
                                expect(bicis[0].code).toEqual(2); 
                                expect(bicis[0].color).toEqual("rojo"); 
                                done();
                            });
                        });

                    });
                });
            });
        });
    });

    describe('Bicicleta.updateByCode', () => {
        it('actualiza una bicicleta', (done) => {
            let aBici = new Bicicleta({code:2, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                let aBici2 = new Bicicleta({code: 2, color:"rojo", modelo:"montaña"});
                Bicicleta.updateByCode(2, {color: "rojo", modelo:"montaña"}, function(err, upBici) {
                    if (err) console.log(err);
                    console.log('UPdated', upBici);
                    Bicicleta.allBicis(function(err, bicis) {
                        expect(bicis.length).toEqual(1);
                        expect(bicis[0].code).toEqual(2); 
                        expect(bicis[0].color).toEqual("rojo"); 
                        expect(bicis[0].modelo).toEqual("montaña"); 
                        done();
                    });
                });
            });
        });
    });


});
// beforeEach(() => {
//     Bicicleta.allBicis = [];
// });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacío', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, 'rojo', 'urbana', [25.4491523,-100.8475396]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
        
//     });
// });

// describe('Bicicleta.findById', () => {
//     it('debe devolver la bici con id 2', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici1 = new Bicicleta(1, "verde", "urbana");
//         var aBici2 = new Bicicleta(2, "azul", "montaña");
//         Bicicleta.add(aBici1);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(2);

//         expect(targetBici.id).toBe(2);
//         expect(targetBici.color).toBe(aBici2.color);
//         expect(targetBici.modelo).toBe(aBici2.modelo);
//     });
// });

// describe('Bicicleta.removeById', () => {
//     it('eliminación de bicis', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var aBici1 = new Bicicleta(1, "verde", "urbana");
//         var aBici2 = new Bicicleta(2, "azul", "montaña");
//         Bicicleta.add(aBici1);
//         Bicicleta.add(aBici2);

//         Bicicleta.removeById(2);
//         expect(Bicicleta.allBicis[0].id).toBe(1);
//         Bicicleta.removeById(1);
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });