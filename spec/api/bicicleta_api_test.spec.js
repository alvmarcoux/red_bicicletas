var mongoose = require('mongoose');

var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:5000/api/bicicletas";


describe('Bicicleta API', () => {
    beforeAll(function(done) {
        let mongoDB = 'mongodb://localhost/testdb';//,  useUnifiedTopology: true
        mongoose.connect(mongoDB, {useNewUrlParser: true }).catch(() => {});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database:');
            done();
        })
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });



    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body) {
                console.log('THE body', body);
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code":10, "color": "rojo", "modelo": "urbana", "lat":25, "lng": -100.1}';
    
            request.post({
                headers: headers, 
                url: base_url + '/create', 
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var result = JSON.parse(body).bicicleta;
                console.log(result);
                expect(result.color).toBe("rojo");
                // expect(result.ubicacion[0]).toBe(25);
                // expect(result.ubicacion[1]).toBe(-100.1);
                done();
            });
        });
    });
    
    describe('POST BICICLETAS / Update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            // var a = new Bicicleta(20, 'Azul', 'Montaña', [25.4491523,-100.8475396]);
            var bici = Bicicleta.createInstance(20, 'Azul', 'Montaña');
            Bicicleta.add(bici, function(err, newBici){
                var biciUpdated = '{ "code":20, "color": "Verde", "Carreras": "urbana", "lat":25.4591523, "lng": -100.8435396}';
                request.post({
                    headers: headers, 
                    url: base_url + '/20/update', 
                    body: biciUpdated
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    // var bicis = JSON.parse(body);
                    // console.log('bicis', bicis);
                    Bicicleta.allBicis(function(err, bicis) {
                        expect(bicis.length).toEqual(1);
                        expect(bicis[0].color).toBe("Verde");
                        done();
                    });                     
                });
            });
        });
    });

    describe('DELETE BICICLETAS', () => {
        it('Status 204', (done) => {
            var bici = Bicicleta.createInstance(10, "azul", "urbana" ); //[25.25, -100.103] 
            Bicicleta.add(bici, function(err, newBici){
                if (err) console.log(err);
                var headers = {'content-type' : 'application/json'};
                request.delete({
                    headers: headers, 
                    url: base_url + '/delete', 
                    body: '{ "code":10 }'
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(204);
                    Bicicleta.allBicis(function(err, bicis) {
                        expect(bicis.length).toEqual(0);
                        done();
                    });
                });
            });
        });
    });

});


