var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis) {
        res.render('bicicletas/index', { bicis: bicis });
    });    
}

exports.bicicleta_create_get = function (req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res){
    var bici = new Bicicleta(req.body);
    // bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici, function(err, newBici) {
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get = function (req, res){
    Bicicleta.findByCode(req.params.id, function (err, fBici) {
        res.render('bicicletas/update', {bici: fBici});
    });
}

exports.bicicleta_update_post = function (req, res){
    var mBici = req.body;
    mBici.code = req.body.code;
    mBici.color = req.body.color;
    mBici.modelo = req.body.modelo;
    Bicicleta.updateByCode(req.params.id, mBici, function(err, upBici) {
        if (err) console.log(err);
        // bici.ubicacion = [ req.body.lat,  req.body.lng];
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_delete_post = function(req, res){
    console.log(req.body);
    Bicicleta.removeByCode(req.body.code, function(error, result){
        res.redirect('/bicicletas');    
    });    
}