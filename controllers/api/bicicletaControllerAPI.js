var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis) {
        res.status(200).json({
            bicicletas: bicis
        });
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta(req.body);
    // bici.ubicacion = [req.body.lat, req.body.lng];
    // bici.createIndex({ ubicacion: [req.body.lat, req.body.lng]});
    // Bicicleta.add(bici).populate('ubicacion').exec(function(err, newBici) {

    Bicicleta.add(bici, function(err, newBici) {
        res.status(200).json({
            bicicleta: newBici
        });
    });
}

exports.bicicleta_update = function(req, res){
    let mBici = {code: req.body.code, color: req.body.color, modelo: req.body.modelo };
    console.log('mBici', mBici);
    Bicicleta.updateByCode(req.params.id, mBici, function(err, upBici) {
    // var bici = Bicicleta.findByCode(req.params.id, function (err, fBici) {
        // fBici.code = req.body.code;
        // fBici.color = req.body.color;
        // fBici.modelo = req.body.modelo;
        // bici.ubicacion = [ req.body.lat,  req.body.lng];
        res.status(200).json({
            bicicleta: upBici
        });
    });
}

exports.bicicleta_delete = function(req, res){
    // console.log('req.body.code', req.body.code);
    Bicicleta.removeByCode(req.body.code, function(error, result){
        res.status(204).send();
    });
}

