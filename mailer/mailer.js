const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth:{
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options); 
} else {
    if (process.env.NODE_ENV == 'staging') {
        console.log('XXXXXXXXXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    }
    else {
        mailConfig = {
            host: 'smtp.ethereal.email', 
            port: 587, 
            auth: {
                user: process.env.ethereal_user,  //'wilfrid64@ethereal.email',
                pass: process.env.ethereal_pwd //'JgMYxmu1Vs6BMUQRSg'
            }
        }
    }
}


const transporter = nodemailer.createTransport(mailConfig);

var Mailer = function(){};

Mailer.send = async function(obj, cb) {
    let info = await transporter.sendMail(obj);    

    console.log("Message sent: %s", info.messageId);
    // {
    //     from: '"Fred Foo 👻" <foo@example.com>', // sender address
    //     to: "alvmarcoux@gmail.com, alvmarcoux@gmail.com", // list of receivers
    //     subject: "Hello ✔", // Subject line
    //     text: "Hello world?", // plain text body
    //     html: "<b>Hello world?</b>", // html body
    //   }    
    cb();
}

// module.exports = nodemailer.createTransport(mailConfig); 
module.exports = Mailer;
